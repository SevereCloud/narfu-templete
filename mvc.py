import sqlite3
import types

NAME_BASE = 'db.sqlite3'

class GoodsModel:
    def get_price(self, name):
        goodslist = self._dbselect(
            'select * from goods where name = ?', [name])
        list = []
        for row in goodslist:
            list.append(row[2])
        return list

    def get_name(self, price):
        goodslist = self._dbselect(
            'select * from goods where price = ?', [price])
        list = []
        for row in goodslist:
            list.append(row[1])
        return list

    def addModel(self, id, name, price):
        connection = sqlite3.connect(NAME_BASE)
        cursor = connection.cursor()
        result = cursor.execute("insert into goods(id, name, price) values (?, ?, ?)", [id, name, price])
        connection.commit()
        connection.close()

    def deleteModel(self, id):
        connection = sqlite3.connect(NAME_BASE)
        cursor = connection.cursor()
        result = cursor.execute("delete from goods where id=?", [id])
        connection.commit()
        connection.close()

    def updateModel(self, id, name, price):
        connection = sqlite3.connect(NAME_BASE)
        cursor = connection.cursor()
        result = cursor.execute("update goods set name=?, price=? where id=?", [ name, price, id])
        connection.commit()
        connection.close()

    def _dbselect(self, query, arg):
        connection = sqlite3.connect(NAME_BASE)
        cursor = connection.cursor()
        result = cursor.execute(query, arg).fetchall()
        connection.commit()
        connection.close()
        return result


class GoodsView:
    def price(self, list, name):
        print('list price of goods for name = {0}'.format(name))
        for element in list:
            print(element)

    def name(self, name, price):
        print('goods with price {0} has name {1}'.format(price, name))


class Controller:
    def get_price(self, name):
        model = GoodsModel()
        view = GoodsView()
        price_data = model.get_price(name)
        return view.price(price_data, name)

    def get_name(self, price):
        model = GoodsModel()
        view = GoodsView()
        name_data = model.get_name(price)
        return view.name(name_data, price)
    
    def test(self):
        model = GoodsModel()
        view = GoodsView()

        price = 30.0
        model.addModel(3,'test', price)
        name_data = model.get_name(price)
        view.name(name_data, price)

        name = 'test2'
        model.updateModel(3, name, 10.0)
        price_data = model.get_price(name)
        view.price(price_data, name)
        
        model.deleteModel(3)

if __name__ == '__main__':
    controller = Controller()
    controller.get_price('milk')
    print("----")
    controller.get_name('10.0')
    print("----")
    controller.test()
