class CPU:
    def __init__(self): print("check CPU")


class Memory:
    def __init__(self): print("check Memory")


class HardDrive:
    def __init__(self): print("check HardDrive")


class BIOS:
    @staticmethod
    def checkCPU(): return CPU()

    @staticmethod
    def checkMemory(): return Memory()

    @staticmethod
    def checkHardDrive(): return HardDrive()


if __name__ == '__main__':
    a = BIOS.checkCPU()
    b = BIOS.checkMemory()
    c = BIOS.checkHardDrive()
