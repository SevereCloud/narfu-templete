from __future__ import generators
import random
# The Project hierarchy cannot be changed:


class Project(object):
    def accept(self, visitor):
        visitor.visit(self)

    def start(self, Engineer):
        print(self, 'start by', Engineer)

    def end(self, ender):
        print(self, 'end by', ender)

    def __str__(self):
        return self.__class__.__name__


class Gladiolus(Project):
    pass


class Runuculus(Project):
    pass


class Chrysanthemum(Project):
    pass


class Visitor:
    def __str__(self):
        return self.__class__.__name__


class Being(Visitor):
    pass


class Engineer(Being):
    pass


class Marketer(Being):
    pass


class Manager(Being):
    pass


class Dwarf(Engineer):
    def visit(self, Project):
        Project.start(self)


class Elf(Marketer):
    def visit(self, Project):
        Project.start(self)



class Troll(Manager):
    def visit(self, Project):
        Project.end(self)


def projectGen(n):
    prjcts = Project.__subclasses__()
    for i in range(n):
        yield random.choice(prjcts)()


if __name__ == '__main__':
    dwarf = Dwarf()
    elf = Elf()
    troll = Troll()
    for project in projectGen(10):
        project.accept(dwarf)
        project.accept(elf)
        project.accept(troll)

